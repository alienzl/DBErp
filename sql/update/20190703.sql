CREATE TABLE `dberp_app` (
  `app_id` int(11) NOT NULL,
  `app_name` varchar(100) NOT NULL,
  `app_access_id` varchar(30) NOT NULL,
  `app_access_secret` varchar(50) NOT NULL,
  `app_url` varchar(100) NOT NULL,
  `app_url_port` varchar(10) NOT NULL DEFAULT '80',
  `app_type` varchar(20) NOT NULL,
  `app_goods_bind_type` varchar(20) DEFAULT NULL COMMENT '商品绑定类型',
  `app_goods_bind` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否启用商品绑定',
  `app_state` tinyint(2) NOT NULL DEFAULT '1',
  `app_add_time` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='绑定电商系统表';

ALTER TABLE `dberp_app`
  ADD PRIMARY KEY (`app_id`),
  ADD KEY `dberp_app_index` (`app_access_id`,`app_access_secret`,`app_state`);

  ALTER TABLE `dberp_app`
  MODIFY `app_id` int(11) NOT NULL AUTO_INCREMENT;


  --
-- 表的结构 `dberp_shop_order`
--

CREATE TABLE `dberp_shop_order` (
  `shop_order_id` int(11) NOT NULL,
  `shop_order_sn` varchar(50) CHARACTER SET utf8 NOT NULL,
  `shop_buy_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `shop_payment_code` varchar(20) CHARACTER SET utf8 NOT NULL,
  `shop_payment_name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `shop_payment_cost` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `shop_payment_certification` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `shop_express_code` varchar(30) CHARACTER SET utf8 NOT NULL,
  `shop_express_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `shop_express_cost` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `shop_order_other_cost` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `shop_order_other_info` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `shop_order_state` tinyint(2) NOT NULL DEFAULT '10',
  `shop_order_discount_amount` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `shop_order_discount_info` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `shop_order_goods_amount` decimal(19,4) NOT NULL,
  `shop_order_amount` decimal(19,4) NOT NULL,
  `shop_order_add_time` int(10) NOT NULL,
  `shop_order_pay_time` int(10) DEFAULT NULL,
  `shop_order_express_time` int(10) DEFAULT NULL,
  `shop_order_finish_time` int(10) DEFAULT NULL,
  `shop_order_message` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `app_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商城订单';

ALTER TABLE `dberp_shop_order`
  ADD PRIMARY KEY (`shop_order_id`),
  ADD KEY `dberp_shop_order_index` (`shop_payment_code`,`shop_express_code`,`shop_order_state`,`app_id`);

ALTER TABLE `dberp_shop_order`
  MODIFY `shop_order_id` int(11) NOT NULL AUTO_INCREMENT;


CREATE TABLE `dberp_shop_order_goods` (
  `order_goods_id` int(11) NOT NULL,
  `shop_order_id` int(11) NOT NULL,
  `goods_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `goods_spec` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `goods_sn` varchar(30) CHARACTER SET utf8 NOT NULL,
  `goods_barcode` varchar(30) DEFAULT NULL,
  `goods_unit_name` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `goods_price` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `goods_type` tinyint(1) NOT NULL DEFAULT '1',
  `buy_num` int(11) NOT NULL,
  `goods_amount` decimal(19,4) NOT NULL DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商城订单商品';

ALTER TABLE `dberp_shop_order_goods`
  ADD PRIMARY KEY (`order_goods_id`),
  ADD KEY `shop_order_goods_index` (`shop_order_id`,`goods_type`,`buy_num`,`goods_amount`);

ALTER TABLE `dberp_shop_order_goods`
  MODIFY `order_goods_id` int(11) NOT NULL AUTO_INCREMENT;


CREATE TABLE `dberp_shop_order_delivery_address` (
  `delivery_address_id` int(11) NOT NULL,
  `delivery_name` varchar(100) NOT NULL,
  `region_info` varchar(50) DEFAULT NULL,
  `region_address` varchar(300) NOT NULL,
  `zip_code` varchar(10) DEFAULT NULL,
  `delivery_phone` varchar(20) NOT NULL,
  `delivery_telephone` varchar(20) DEFAULT NULL,
  `delivery_number` varchar(30) DEFAULT NULL,
  `delivery_info` varchar(500) DEFAULT NULL,
  `shop_order_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商城订单配送地址';

ALTER TABLE `dberp_shop_order_delivery_address`
  ADD PRIMARY KEY (`delivery_address_id`),
  ADD KEY `shop_order_id` (`shop_order_id`),
  ADD KEY `dilivery_number` (`delivery_number`);

ALTER TABLE `dberp_shop_order_delivery_address`
  MODIFY `delivery_address_id` int(11) NOT NULL AUTO_INCREMENT;
